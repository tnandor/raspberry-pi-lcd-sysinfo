import I2C_LCD_driver
import sysInfo

mylcd = I2C_LCD_driver.lcd()

r1 = "IP: "
r1 += sysInfo.get_wlan0_ip()

r2 = sysInfo.getCPUPercentage()
r2 += " "
r2 += sysInfo.getDiskPerc()

mylcd.lcd_display_string(r1, 1)
mylcd.lcd_display_string(r2, 2)

